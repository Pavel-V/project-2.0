const getComments = async () => {
    const body = await fetch('https://gorest.co.in/public/v2/comments')
    const response = await body.json()

    return response

}

const getPosts = async () => {
    const body = await fetch('https://gorest.co.in/public/v2/posts')
    const response = await body.json()

    return response

}

const getUsers = async () => {
    const body = await fetch('https://gorest.co.in/public/v2/users')
    const response = await body.json()

    return response

}