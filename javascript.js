/* const array = [{
    'id': 1820,
    'post_id': 1758,
    'name': 'Anal Guneta',
    'email': 'guneta_anal@schroeder.io',
    'body': 'Reprehenderit similique qui. Pariatur consequuntur suscipit. Et recusandae rerum. Explicabo officia delectus.',
  }, {
    'id': 1819,
    'post_id': 1757,
    'name': 'Menka Pothuvaal',
    'email': 'menka_pothuvaal@will.co',
    'body': 'Aperiam aliquam nisi. Est dolore occaecati. Sapiente officiis quis. Aut corrupti accusantium.',
  }, {
    'id': 1811,
    'post_id': 1749,
    'name': 'Sarisha Bhattacharya',
    'email': 'bhattacharya_sarisha@weber.org',
    'body': 'Optio quibusdam porro. Tempore nihil sit.',
  }, {
    'id': 1810,
    'post_id': 1748,
    'name': 'Dhanalakshmi Varman',
    'email': 'dhanalakshmi_varman@satterfield.name',
    'body': 'Commodi cum autem.',
  }, {
    'id': 1809,
    'post_id': 1748,
    'name': 'Darshwana Gupta V',
    'email': 'v_darshwana_gupta@rutherford.org',
    'body': 'Voluptas quia beatae. Omnis corporis harum. Possimus est autem. Ea quo dolor.',
  }, {
    'id': 1808,
    'post_id': 1747,
    'name': 'Aanandinii Naik',
    'email': 'naik_aanandinii@fahey-kreiger.com',
    'body': 'Sunt sed debitis. Veritatis rem sed.',
  }, {
    'id': 1807,
    'post_id': 1746,
    'name': 'Gemine Tandon',
    'email': 'gemine_tandon@huel.org',
    'body': 'Et reiciendis quas. Quibusdam in est. Et cum harum. Est officiis necessitatibus.',
  }, {
    'id': 1806,
    'post_id': 1745,
    'name': 'Rohit Bhat Sr.',
    'email': 'rohit_sr_bhat@mayert.biz',
    'body': 'Illo est perspiciatis. Deserunt ut ea.',
  }, {
    'id': 1805,
    'post_id': 1744,
    'name': 'Amb. Esha Nayar',
    'email': 'amb_nayar_esha@price.name',
    'body': 'Similique qui tempora. Porro rerum ipsa.',
  }, {
    'id': 1804,
    'post_id': 1743,
    'name': 'Deependra Talwar',
    'email': 'deependra_talwar@hauck.co',
    'body': 'Voluptatum est harum. Quae facere corporis. Voluptas omnis sed. Cumque corporis sed.',
  }, {
    'id': 1802,
    'post_id': 1741,
    'name': 'Krishnadasa Desai IV',
    'email': 'desai_krishnadasa_iv@fritsch-schaefer.name',
    'body': 'Autem delectus sit. Atque aut aut. Nihil ullam est.',
  }, {
    'id': 1801,
    'post_id': 1739,
    'name': 'Rupinder Kaniyar',
    'email': 'rupinder_kaniyar@kirlin.net',
    'body': 'Ratione sint voluptatum.',
  }, {
    'id': 1800,
    'post_id': 1737,
    'name': 'Rahul Kaul',
    'email': 'kaul_rahul@erdman.info',
    'body': 'Ut unde consequuntur. Incidunt accusamus beatae. Culpa omnis laudantium. Enim recusandae consequuntur.',
  }, {
    'id': 1799,
    'post_id': 1735,
    'name': 'Smriti Arora III',
    'email': 'arora_iii_smriti@frami.name',
    'body': 'Sed repellendus rem. Aut veritatis nostrum. Libero odit voluptas. Ducimus earum ex.',
  }, {
    'id': 1798,
    'post_id': 1735,
    'name': 'Msgr. Lavanya Mehrotra',
    'email': 'mehrotra_lavanya_msgr@doyle.info',
    'body': 'Molestiae modi sit. Rerum eaque assumenda. Nulla est quam.',
  }, {
    'id': 1797,
    'post_id': 1734,
    'name': 'Tushar Singh',
    'email': 'tushar_singh@runte.io',
    'body': 'Labore alias occaecati.',
  }, {
    'id': 1796,
    'post_id': 1733,
    'name': 'Bheeshma Khanna',
    'email': 'bheeshma_khanna@romaguera.name',
    'body': 'Neque qui alias. Sit quaerat est. Commodi voluptatum dolor.',
  }, {
    'id': 1795,
    'post_id': 1732,
    'name': 'Jaya Dwivedi',
    'email': 'dwivedi_jaya@hettinger-goldner.name',
    'body': 'Minima magni animi. Quos quis sed. Sed est voluptatem.',
  }, {
    'id': 1794,
    'post_id': 1729,
    'name': 'Chapala Menon',
    'email': 'menon_chapala@wyman-jacobson.name',
    'body': 'Maxime sunt autem. Delectus non voluptates. Repudiandae aspernatur provident.',
  }, {
    'id': 1793,
    'post_id': 1729,
    'name': 'Aalok Tagore',
    'email': 'tagore_aalok@kuhlman.co',
    'body': 'Nemo unde et. Quod velit natus.',
  }] */

  let post = document.getElementById('post-wrapper')

  let header = document.getElementById('header')

  function addH(array) {

    array.map((text) => {
    
        let title = document.createElement('div');
        title.innerText = text.name;
        
        let subtitle = document.createElement('div');
        subtitle.innerText = text.email;
       

        let wrap = document.createElement('div')
        wrap.appendChild(title).classList.add('header')
        wrap.appendChild(subtitle).classList.add('subtitle')
        post.appendChild(wrap)
        wrap.classList.add('wrapper')
        
        let paragraph = document.createElement('p')
        paragraph.innerText = text.body;
        wrap.appendChild(paragraph)
        paragraph.classList.add('paragraph')
        
    }) 
    }
    //addH(array)

    async function renderPosts() {
      const comments = await getComments()
      addH(comments)
    }

    renderPosts()

    /* const response = fetch('https://gorest.co.in/public/v2/comments').then((response) => {
      response.json().then((res) => {
        console.log(res)
        addH(res)
      })
    })

    console.log(response) */

    /* const pro = new Promise( (resolve, reject) => {
      console.log(1)
      setTimeout(() => {
        console.log(2)
        reject('pizda')
      }, 3000)
    }).then((res) => {
      console.log(3, res)
    }).catch((res) => {
      console.log(res)
    }) */