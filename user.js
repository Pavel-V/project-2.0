let users = [];
let post = document.getElementById('wrapper')

const getUsers = async () => {
    const body = await fetch('https://gorest.co.in/public/v2/users')
    const response = await body.json()

    return response

}

async function renderUsers() {
    users = await getUsers()
    users.map((user) => {
        const str = `<a href='/user/${user.id}'>${user.name}</a> <br>`
        post.insertAdjacentHTML('afterbegin', str)
    })
    
}
renderUsers()